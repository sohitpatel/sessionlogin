<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    //

     //Google Login

     public function redirectToGoogle(){
        return Socialite::driver('google')->redirect();

    }

    //Google callback

    public function handleGoogleCallback()
    {


        $user =Socialite::driver('google')->user();

        $this->_registerOrLoginUser($user);

        //return home after loging

         return redirect('/dashboard');
    }

    //Facebook Login

    public function redirectToFacebook(){

        return Socialite::driver('facebook')->redirect();


    }

    //Facebook callback

    public function handleFacebookCallback()
    {


        $user =Socialite::driver('facebook')->user();

        $this->_registerOrLoginUser($user);

        //return home after loging

        return redirect('/dashboard');
    }

    //Github Login

    public function redirectToGithub(){

        return Socialite::driver('github')->redirect();

    }

    //Github callback

    public function handleGithubCallback()
    {
        $user =Socialite::driver('github')->user();

        $this->_registerOrLoginUser($user);

        //return home after loging

        return redirect('/dashboard');
    }

    //Method for all login

    protected function _registerOrLoginUser($request){


        $user =User::Where('email', $request->email)->first();

        if(!$user){

            $user = new User;
            $user->name =$request->name;
            $user->email= $request->email;
            $user->provider_id =$request->id;
            $user->picture = $request->avatar;
            $user->save();
        }

        Auth::login($user);

    }
}
